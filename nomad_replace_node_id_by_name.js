
function replace_a_node_id_by_name(node) {
    // If element is a
    if (node.nodeType === Node.ELEMENT_NODE && node.nodeName.toLowerCase() === 'a') {
        // If a has content that is uuid
        if (node.textContent.match(/[0-9a-fA-F]{6,}/)) {
            var href = node.getAttribute('href');
            // If a href= points to ui/clients/uuid
            if (href && href.match(/\/ui\/clients\/[0-9a-fA-F-]{36}/)) {
                var parent = node.parentElement;
                // If the parent is span with aria-label
                if (parent && parent.tagName.toLowerCase() === 'span' && parent.hasAttribute('aria-label')) {
                    // Set the a content to aria-label of span label.
                    var ariaLabel = parent.getAttribute('aria-label');
                    node.textContent = ariaLabel;
                }
            }
        }
    }
}

function replace_span_node_id_by_name(node) {
    // If element is span, handle all a elements inside. Span may be updated after a.
    if (node.nodeType === Node.ELEMENT_NODE && node.nodeName.toLowerCase() === 'span') {
        if (node.hasAttribute('aria-label')) {
            for (var child of node.children) {
                replace_a_node_id_by_name(child);
            }
        }
    }
}

(function() {
    'use strict';
    function log(a) {
        console.log("NOMADNODESWAP: " + a);
    }
    var observer = new MutationObserver(function(mutations) {
        for (var mutation of mutations) {
            if (mutation.type === "childList") {
                mutation.addedNodes.forEach(replace_a_node_id_by_name);
            } else if (mutation.type === "attributes") {
                replace_span_node_id_by_name(mutation.target);
            }
        }
    });
    observer.observe(document.body, { childList: true, subtree: true, attributes: true });
    // Object.keys(Ember).forEach((prop)=> console.log(prop));
    // Ember.run.debounce('afterRender', modifyContent, 2000);
    // modifyContent();
    // Run the function after the DOM is fully loaded
    // document.addEventListener('DOMContentLoaded', modifyContent);
})();
